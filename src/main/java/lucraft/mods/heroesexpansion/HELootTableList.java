package lucraft.mods.heroesexpansion;

import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootTableList;

public class HELootTableList {

    public static ResourceLocation NORSE_VILLAGE_HOUSE = LootTableList.register(new ResourceLocation(HeroesExpansion.MODID, "norse_village_house"));
    public static ResourceLocation CHITAURI_LOOT = LootTableList.register(new ResourceLocation(HeroesExpansion.MODID, "chitauri_loot"));
    public static ResourceLocation CRASHED_KREE_SHIP = LootTableList.register(new ResourceLocation(HeroesExpansion.MODID, "crashed_kree_ship"));
    public static ResourceLocation KREE = LootTableList.register(new ResourceLocation(HeroesExpansion.MODID, "kree"));

}
